# Lazio region map

The main target of this project is to display different types of data on lazio region map using javascript and different methods for data classification.

## Getting started

First of all, you need a simple server that can serve static files. For example [http-server](https://www.npmjs.com/package/http-server).

'http server' could be installed via npm:

```sh
npm install http-server -g
```

To run the server, go to the project folder and enter this command into the terminal:

```sh
http-server -c-1
```


