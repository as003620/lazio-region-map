'use strict';

// colour range
const START_RED_VALUE = 19;
const END_RED_VALUE = 200;
const RED_RANGE_LENGTH = END_RED_VALUE - START_RED_VALUE;

const BASE_COLORS = {
  r: 19, 
  g: 41,
  b: 64,
};

const INIT_DATA = {
  method: 'Proportional',
  numOfIntervals: 5,
  lazioMapDataType: 'total_accidents',
  lazioMapFeature: null,
};

const LAZIO_DATA_TYPE_OPTIONS = [
  ['day_of_the_week', 'day of the week'],
  ['intersection','intersection'],
  ['location','location'],
  ['road_user','road user'],
  ['time_of_day','time of day'],
  ['total_accidents','total accidents'],
  ['type','type of accident'],
  ['weather','weather']
];

const mapDataStore = createDataStore(INIT_DATA);


(function(){
  const mapData = mapDataStore.getData();
  const mapElement = document.getElementById('map');
  const btnList = document.getElementsByClassName('choose-province-btn');
  Array.prototype.forEach.call(btnList, btn => btn.addEventListener('click', municBtnHandler));
  mapElement.addEventListener('load', createIframeLoadHandler());
  
  const formMapControl = document.getElementById('map-controls');
  formMapControl.addEventListener('change', formControlHandler);

  const intervalSelect = document.getElementById('interval-select');
  intervalSelect.value = mapData.numOfIntervals;
  
  document.getElementById('Proportional').checked = true;
  intervalSelect.disabled = true; // since method is proportional
  
  // Disable select for intervals if method is "Proportional"
  document.getElementById('method-control').addEventListener('change', (e) => {
    const { value } = e.target;
    if (value === 'Proportional') intervalSelect.disabled = true;
    else intervalSelect.disabled = false;
  });
})();

/****************** EVENT HANDLERS ************************************/


function createIframeLoadHandler() {
  let postAction = null;
  return (e) => {
    const iframeElement = e.target;
    if (postAction) postAction();
    const matched = iframeElement.src.match(/\/(\w+)\.html$/);
    const internalDocument = iframeElement.contentDocument;
    const mapData = mapDataStore.getData();
    const method = getMethodById(mapData.method);

    switch(matched[1]) {
      case 'CEREMSS_LAZIO':
        let dataSet = null;
        const selectSubDataContainer = document.getElementById('lazio-sub-data-type');

        axios(`./jsonData/all_${mapData.lazioMapDataType}.json`)
        .then(processLazioIncomingData)
        .then(receivedData => {
          dataSet = receivedData;
          // selecting first subDataType in data set as the default option for select
          const defaultVal = Object.keys(dataSet)[0];
          mapDataStore.setProp('lazioMapFeature', defaultVal);

          method(internalDocument, dataSet[defaultVal], mapData.numOfIntervals);
          // display select for subclasses
          renderLazioSubDataSelect(selectSubDataContainer, dataSet, defaultVal);
        }).catch(requestErrorHandler);  
      
        // show data type select
        const selectContainer = document.getElementById('lazio-data-type');
        const props = {
          options: LAZIO_DATA_TYPE_OPTIONS,
          labelText: 'Data type: ',
          defaultValue: mapData.lazioMapDataType,
          name: 'lazioMapDataType',
          selectId: 'lazioMapDataType',
        };
        renderSelect(selectContainer, props);

        let prevLazioMapDataType = mapData.lazioMapDataType;
        const unsubscribeFromStore = mapDataStore.subscribe((controlData) => {
          const method = getMethodById(controlData.method);
          if (prevLazioMapDataType !== controlData.lazioMapDataType) {
            // if data type is changed then load data
            // after that render the select
            axios(`./jsonData/all_${controlData.lazioMapDataType}.json`)
            .then(processLazioIncomingData)
            .then(receivedData => {
              dataSet = receivedData;
              // selecting first subDataType in data set as default option for select
              const defaultVal = Object.keys(dataSet)[0];
              mapDataStore.setProp('lazioMapFeature', defaultVal);
              method(internalDocument, receivedData[defaultVal], controlData.numOfIntervals);
              // display select for subclasses
              renderLazioSubDataSelect(selectSubDataContainer, dataSet, defaultVal);
            }).catch(requestErrorHandler);
            prevLazioMapDataType = controlData.lazioMapDataType;
          } else {
            if (dataSet === null) return;
            method(internalDocument, dataSet[controlData.lazioMapFeature], controlData.numOfIntervals);
          }
        });
        // postAction callback will be executed each time when map changes 
        postAction = () => {
          unsubscribeFromStore();
          // remove selects for lazio map
          removeChildrenIn(selectContainer);
          removeChildrenIn(selectSubDataContainer);
        }
        
        // making provinces behave as links
        const provinceList = internalDocument.getElementsByClassName('svgarea');
        Array.prototype.forEach.call(provinceList, (el) => el.addEventListener('click', municClickHandler));
        break;
      case 'p056':
      case 'p057':
      case 'p058':
      case 'p059':
      case 'p060':
        axios(`./jsonData/${matched[1].slice(1)}_total_accidents.json`).then(response => {
          const dataSet = {};
          response.data.features.forEach(f => {
            const prop = f.properties;
            dataSet[prop.reg + prop.pro + prop.com] = prop.p_inc;
          });
          method(internalDocument, dataSet, mapDataStore.getData().numOfIntervals);
          postAction = mapDataStore.subscribe(data => {
            const method = getMethodById(data.method);
            method(internalDocument, dataSet, data.numOfIntervals);
          });
        }).catch(requestErrorHandler);
        break;
      default: throw new Error(`Unknown region name: ${matched[1]}`);
    };
  };
}

/**
 * An event handler that changes the content of iframe
 * according to the button which fires event. 
 * @param {Object} e click event object
 */
function municBtnHandler (e) {
  const mapElement = document.getElementById('map');
  const url =  mapElement.src.replace(/\/\w+\.html$/,'/' + e.target.name + '.html');
  mapElement.src = url;
}


/**
 * 
 * @param {Object} e click event object
 */
function municClickHandler (e) {
  const mapElement = document.getElementById('map');
  const url = mapElement.src.replace(/\/\w+\.html$/,'/' + e.currentTarget.id + '.html');
  mapElement.src = url;
}

function formControlHandler(e) {
  const { name, value } = e.target;

  mapDataStore.setProp(name, value);
  
}

function requestErrorHandler (e) {
  throw e;
}

/**************************** CLASSIFICATION METHOD ************************************/


/**
 * Function displays 'dataSet' proportionally on a map that is located in the DOM of 'mapDocument'
 * @param {Object} mapDocument instance of Document that corresponds to the internal content of the iframe  
 * @param {Objet} dataSet data that should be displayed. Keys of the object are ids of DOM elements
 */
function showDataProportion(mapDocument, dataSet) {
  if (!mapDocument || !dataSet) return null;
  
  const values = Object.values(dataSet);
  const maxVal = Math.max(...values);
  const minVal = Math.min(...values);
  const lengthOfRange = maxVal - minVal;

  Object.entries(dataSet).forEach(pair => {
    const newRedValue = Math.round((pair[1] - minVal) / lengthOfRange * RED_RANGE_LENGTH) + START_RED_VALUE;
    const mapElement = mapDocument.getElementById(pair[0]);
    if (!mapElement) throw new Error(`Can\'t find map element with id: ${pair[0]}`);

    const currFill = mapElement.style.fill;
    const newFill = currFill.replace(/rgb\((\d+), (\d+), (\d+)\)/, `rgb(${newRedValue}, $2, $3)`);

    mapElement.style.fill = newFill;
  });
  const data = {
    dataDescr: 'test data',
    startColor: `rgb(${START_RED_VALUE}, ${BASE_COLORS.g}, ${BASE_COLORS.b})`,
    endColor: `rgb(${END_RED_VALUE}, ${BASE_COLORS.g}, ${BASE_COLORS.b})`,
    startValue: minVal,
    endValue: maxVal,
  }
  renderGradientLegend(mapDocument.getElementById('legend'), [data])
}

function showDataEqualIntervals(mapDocument, dataSet, numOfIntervals = 5) {
  if (!mapDocument || !dataSet) return null;
  numOfIntervals = +numOfIntervals;
  
  const values = Object.values(dataSet);
  const maxVal = Math.max(...values);
  const minVal = Math.min(...values);
  const lengthOfRange = maxVal - minVal;
  // We should increase the length of range by 1
  // because maxVal should be less than upper bound
  const lengthOfInterval = (lengthOfRange + 1) / numOfIntervals;
  
  const redVariation = getRedVariation(START_RED_VALUE, END_RED_VALUE, numOfIntervals);
  const redGradientArr = redVariation.map(rVal => [rVal, BASE_COLORS.g, BASE_COLORS.b])

  Object.entries(dataSet).forEach(pair => {
    const intervalNo = Math.floor((pair[1] - minVal) / lengthOfInterval);
    let newRedColor = redGradientArr[intervalNo];

    const mapElement = mapDocument.getElementById(pair[0]);
    if (!mapElement) throw new Error(`Can\'t find map element with id: ${pair[0]}`);
    const newFill = `rgb(${newRedColor[0]}, ${newRedColor[1]}, ${newRedColor[2]}`;

    mapElement.style.fill = newFill;
  });
  
  // creating legend data
  const legendData = redGradientArr.map((color, i) => {
    const entity = {};
    entity.color = `rgb(${color[0]}, ${color[1]}, ${color[2]}`;
    entity.from = Math.round(minVal + i * lengthOfInterval);
    entity.to = Math.round(entity.from + lengthOfInterval - 1);
    return entity;
  });

  renderLegend(mapDocument.getElementById('legend'), legendData);

}

function showDataQuantile(mapDocument, dataSet, numOfIntervals = 5) {
  if (!mapDocument || !dataSet) return null;
  numOfIntervals = +numOfIntervals;
  
  const entriesDS = Object.entries(dataSet);
  const numPerInterval = Math.round(entriesDS.length / numOfIntervals); // number of entries per interval
  let numOfColorIntervals = numOfIntervals;
  if (entriesDS.length < numOfIntervals) {
    console.warn('Warning in quantile method: Number of data entities less than number of intervals!');
    numOfColorIntervals = entriesDS.length;
  }
  entriesDS.sort((a, b) => a[1] - b[1]);

  const redGradientArr = getRedVariation(START_RED_VALUE, END_RED_VALUE, numOfColorIntervals);
  const legendData = [];
  
  entriesDS.forEach((pair, i) => {
    const interval = Math.floor(i / numPerInterval);
    const mapElement = mapDocument.getElementById(pair[0]);
    if (!mapElement) throw new Error(`Can\'t find map element with id: ${pair[0]}`);
    let redVal;
    if (redGradientArr[interval]) {
      redVal = redGradientArr[interval];
    } else {
      redVal = redGradientArr[interval - 1];
    }

    const currFill = mapElement.style.fill;
    const newFill = currFill.replace(/rgb\((\d+), (\d+), (\d+)\)/, `rgb(${redVal}, $2, $3)`);

    mapElement.style.fill = newFill;

    // generating legend data
    if (!(i % numPerInterval) && legendData.length !== numOfIntervals) {
      const entity = {};
      entity.color = newFill;
      entity.from = pair[1];

      if (entriesDS[i + numPerInterval - 1] && ((legendData.length + 1) !== numOfIntervals)) {
        entity.to = entriesDS[i + numPerInterval - 1][1];
      } else {
        entity.to = entriesDS[entriesDS.length - 1][1];
      }
      
      legendData.push(entity);
    }
  });
  renderLegend(mapDocument.getElementById('legend'), legendData);
};


/********************* LEGEND RENDERS ****************************/


/**
 * function takes data and renders the legend of a map
 * @param {HTMLElement} domRoot - parent element that will contain the legend
 * @param {Array} arrData array of Objects
 * @param {String} arrData.color
 * @param {String|Number} arrData.from
 * @param {String|Number} arrData.to
 */
function renderLegend(domRoot, arrData) {
  if (!domRoot || !arrData) return null;
  // removing content of domRoot
  while (domRoot.firstChild) {
    domRoot.removeChild(domRoot.firstChild);
  } 
  
  const legendContainer = document.createElement('table');
  legendContainer.className = 'legend-interval-method';
  arrData.forEach(entity => {
    const row = document.createElement('tr');
    const entityColor = document.createElement('td');
    entityColor.style.backgroundColor = entity.color;
    const entityDef = document.createElement('td');
    entityDef.className = 'legend-definition';
    const defString = entity.from !== entity.to ? `${entity.from} - ${entity.to}` : entity.from;

    entityDef.textContent = defString;

    row.appendChild(entityColor);
    row.appendChild(entityDef);

    legendContainer.appendChild(row);
  });
  domRoot.appendChild(legendContainer);
}
/**
 * @param {HTMLElement} domRoot - parent element that will contain the legend
 * @param {Object[]} arrData
 * @param {String} data.dataDescr
 * @param {String} data.startColor 
 * @param {String} data.endColor
 * @param {String|Number} data.startValue
 * @param {String|Number} data.endValue  
 */
function renderGradientLegend(domRoot, arrData) {
  if (!domRoot || !arrData) return null;

  // removing content of domRoot
  while (domRoot.firstChild) {
    domRoot.removeChild(domRoot.firstChild);
  }
  
  const legendContainer = document.createElement('table');
  legendContainer.className = 'legend-proportional-method';
  arrData.forEach(e => {
    const mainRow = document.createElement('tr');
    const tdColor = document.createElement('td');
    tdColor.colSpan = '2';
    tdColor.style.backgroundImage = `linear-gradient(to right, ${e.startColor} , ${e.endColor})`;
    const tdDescr = document.createElement('td');
    tdDescr.className = 'legend-definition';

    tdDescr.textContent = e.dataDescr;

    mainRow.appendChild(tdColor);
    mainRow.appendChild(tdDescr);


    const definitionRow = document.createElement('tr');
    const startTd = document.createElement('td');
    const endTd = document.createElement('td');

    startTd.textContent = e.startValue;
    endTd.textContent = e.endValue;

    definitionRow.appendChild(startTd);
    definitionRow.appendChild(endTd);
    definitionRow.appendChild(document.createElement('td'));


    legendContainer.appendChild(mainRow);
    legendContainer.appendChild(definitionRow);
  });
  domRoot.appendChild(legendContainer);
}

/******************** RENDERS ****************************/

/**
 * 
 * @param {HTMLElement} rootElement 
 * @param {Object} props specifies meta data
 * @param {Array[][]} props.options options to be rendered for select 
 * @param {String} props.labelText
 * @param {String} props.selectId id to be assigned to the select
 * @param {String} props.name 
 * @param {String} props.defaultValue
 */
function renderSelect(rootElement, props) {
  if (!rootElement || !props) return null;

  const select = document.createElement('select');
  if (props.labelText) {
    const label = document.createElement('label');
    label.textContent = props.labelText;
    if (props.selectId) {
      label.setAttribute('for', props.selectId);
      select.setAttribute('id', props.selectId);
    }
    rootElement.appendChild(label);
  }
  
  select.setAttribute('name', props.name);

  props.options.forEach((o) => {
    const optionElement = document.createElement('option');
    if (props.defaultValue === o[1]) {
      optionElement.setAttribute('selected', '');
    }
    optionElement.textContent = o[1];
    optionElement.setAttribute('value', o[0]);

    select.appendChild(optionElement);
  });

  rootElement.appendChild(select);
}

function renderLazioSubDataSelect(rootElement, dataSet, defaultVal) {
  if (!rootElement || !dataSet) return null;
  
  // if rootElement isn't empty - remove each child in it
  removeChildrenIn(rootElement);
  const options = Object.keys(dataSet).map(k => [k, k]);
  
  const props = { 
    options,
    name: 'lazioMapFeature',
    labelText: 'Feature: ',
    defaultValue: defaultVal,
    selectId: 'lazio-map-feature',
  };
  renderSelect(rootElement, props);
}


/******************** UTILS ******************************/


function getRedVariation(startVal, endVal, numOfIntervals) {
  if (!startVal || !endVal || !numOfIntervals) return;
  const rangeLength = endVal - startVal;

  const lengthOfColorInterval = rangeLength / numOfIntervals;
  const redGradientArr = [];

  for (let i = 0, newRedVal; i < numOfIntervals; i++) {
    newRedVal = startVal + Math.round(i * lengthOfColorInterval + lengthOfColorInterval / 2);
    redGradientArr[i] = newRedVal;
  }

  return redGradientArr;
}


function getMethodById(idString) {
  switch (idString) {
    case 'Proportional': return showDataProportion;
    case 'Equal_intervals': return showDataEqualIntervals;
    case 'Quantile': return showDataQuantile;
    default: throw new Error('Unknown method id: ', idString);
  }
}


function createDataStore(initialData) {
  const data = initialData;
  let listenersList = [];

  return {
    subscribe(listener) {
      listenersList.push(listener);
      return function unsubscribe() { listenersList = listenersList.filter(l => l !== listener) }
    },

    setProp(propName, propValue) {
      data[propName] = propValue;
      listenersList.forEach(listener => listener({ ...data }));
      return propValue;
    },

    getData() {
      return { ...data };
    }
  };
}

function getIdByProvinceName(provinceName) {
  switch (provinceName) {
    case 'Viterbo': return 'p056';
    case 'Rieti': return 'p057';
    case 'Roma': return 'p058';
    case 'Latina': return 'p059';
    case 'Frosinone': return 'p060';
    default: throw new Error('Unknown province name: ', provinceName);
  }
}

function processLazioIncomingData(response) {
  let dataSet = {};
  const { features } = response.data;
  const propSample = features[0].properties;

  if (propSample.p_inc) {
    dataSet.total = {};
    features.forEach(f => {
      const p = f.properties;
      dataSet.total[getIdByProvinceName(p.nome)] = +p.p_inc;
    });
  } else {
    const prefix = 'numinc_';
    dataSet = (Object.keys(propSample)
    .filter(key => key.startsWith(prefix))
    .map(key => key.slice(prefix.length))
    .reduce((acc, key) => {
      acc[key] = {};
      return acc;
    }, {}));

    const dataSetKeys = Object.keys(dataSet);
    features.forEach(f => {
      const p = f.properties;
      dataSetKeys.forEach((k) => {
        dataSet[k][getIdByProvinceName(p.nome)] = +p[prefix + k];
      });
    });
  }
  return dataSet;
}

/**
 * function removes all children of node parent
 * @param {Node} node 
 */
function removeChildrenIn(node) {
  if (!node) return null;

  while (node.firstChild){
    node.removeChild(node.firstChild);
  }
  return true;
}
